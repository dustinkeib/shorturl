import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Links } from '../../imports/collections/links';

class LinkList extends Component {
  handleClick(event) {
    Meteor.call('links.remove', this.token)
  }

  renderRows() {
    return this.props.links.map(link => {
      const { url, clicks, token } = link;
      this.token = token

      if (Meteor.isProduction) {
        var shortLink = `https://dk-shorturl.herokuapp.com/${token}`;
      } else {
        var shortLink = `http://localhost:3000/${token}`;
      }

      return (
        <tr key={token}>
          <td>{url}</td>
          <td>
            <a href={shortLink}>{shortLink}</a>
          </td>
          <td>
            {clicks}
          </td>
          <td>
            <span className='deleteButton' onClick={this.handleClick.bind(this)}>
              <i className="fa fa-trash-o" aria-hidden="true"></i>
            </span>
          </td>
        </tr>
      );
    });
  }

  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>URL</th>
            <th>Address</th>
            <th>Clicks</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {this.renderRows()}
        </tbody>
      </table>
    );
  }
}

export default createContainer(() => {
  Meteor.subscribe('links');

  return { links: Links.find({}).fetch() };
}, LinkList);
