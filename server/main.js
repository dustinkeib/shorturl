import { Meteor } from 'meteor/meteor';
import { Links } from '../imports/collections/links'
import { WebApp } from 'meteor/webapp'
import ConnectRoute from 'connect-route'

// Happens on startup
Meteor.startup(() => {

  if (Meteor.isDevelopment) {
    var runMode = 'Dev'
  } else if (Meteor.isProduction) {
    var runMode = 'Prod'
  } else if (Meteor.isTest) {
    var runMode = 'Test'
  } else if (Meteor.isAppTest) {
    var runMode = 'AppTest'
  } else {
    var runMode = 'Undefined'
  }
  console.log(`Running in ${runMode}`)

  // Publish Links collection
  Meteor.publish('links', function() {
    return Links.find({})
  })
});

// Redirect traffic to http://localhost:3000/{token}
// Args: Request, Result, Next middleware to run
function onRoute(req, res, next) {
  // Find matching link in Links Collection
  const link = Links.findOne({ token: req.params.token })

  // If matching link, redirect to long url and inc clicks
  // https://docs.mongodb.com/manual/reference/operator/update/
  if (link) {
    Links.update(link, { $inc: { 'clicks': 1 }})
    res.writeHead(307, {'Location': link.url})
    res.end()
  } else {
    // If not matching link, send to normal react app
    next()
  }
}

// Middleware to re-route shorturl to long url
// http://localhost:3000/             NO Match
// http://localhost:3000/some/stuff   NO Match
// http://localhost:3000/abcd         MATCH!!
const middleware = ConnectRoute(function(router) {
  router.get('/:token', onRoute)
})

// Built in meteor webserver to reroute url
WebApp.connectHandlers.use(middleware)
