import React from 'react'

const Header = () => {
  return(
    <nav className="nav navbar-default">
      <div className="nav navbar-header">
        <a className="navbar-brand">ShortURL</a>
      </div>
    </nav>
  )
}

export default Header
